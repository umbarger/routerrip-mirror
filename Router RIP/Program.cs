﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Router_RIP
{
    class Program
    {
        public static bool enableNodeFailure = true;
        public static int numRouterPairs = 10;
        static void Main(string[] args)
        {
            List<Thread> routerThreads = new List<Thread>();
            NetworkGraph netGraph = new NetworkGraph(numRouterPairs);
            
            Console.WriteLine("Network Topology:");
            Console.Write(netGraph.ToString());

            Console.WriteLine("\n\nStarting Sim...");

            try{
                Thread.Sleep(5000);

                // Generate the threads
                foreach( Node n in NetworkGraph.GetNetworkGraph() )
                {

                    Thread thisRouter = new Thread(new ThreadStart(n.Run));
                    routerThreads.Add( thisRouter );
                }

                // Start the threads
                foreach( Thread t in routerThreads )
                    t.Start();

                // Join the threads
                foreach( Thread t in routerThreads )
                    t.Join();

                Console.WriteLine("All routers have failed. Sim concluded.");
            } catch( ArgumentNullException e ) {}

            Console.Read();
        }
    }
}
