﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Router_RIP
{
    class RoutingTable
    {
        private Node currentNode;

        // Destination data
        private List<String> destinationIP = new List<String>();
        private List<String> subnetMask = new List<String>();
        private List<String> nextHopIP = new List<String>();
        private List<int> distanceMetric = new List<int>();
        private List<Node> neighborNodes = new List<Node>();
        private List<int> destinationTimes = new List<int>();

        private const int infinity = int.MaxValue;

        public RoutingTable( Node n, HashSet<Node> netGraph )
        {
            this.currentNode = n;
            
            HashSet<Edge> neighborsEdges = n.GetEdges();

            foreach( Edge e in neighborsEdges )
            {
                Node x = e.GetX();
                Node y = e.GetY();

                Node neighbor;

                if (currentNode.Equals(x))
                    neighbor = y;
                else
                    neighbor = x;

                neighborNodes.Add(neighbor);
            }

            foreach( Node r in netGraph )
            {
                String curIP = r.GetAddress();

                if( !curIP.Equals( currentNode.GetAddress()) && !neighborNodes.Contains( r ) )
                {
                    destinationIP.Add(curIP);
                    subnetMask.Add(n.GetMask());
                    distanceMetric.Add(infinity);
                    nextHopIP.Add("0.0.0.0");
                }
                else if ( neighborNodes.Contains(r) )
                {
                    int weight = 0;

                    foreach( Edge e in neighborsEdges )
                    {
                        Node x = e.GetX();
                        Node y = e.GetY();

                        if( x.Equals(r) || y.Equals(r) )
                        {
                            weight = e.GetWeight();
                        }
                    }

                    destinationIP.Add(curIP);
                    subnetMask.Add(n.GetMask());
                    distanceMetric.Add(weight);
                    nextHopIP.Add(n.GetAddress());
                    destinationTimes.Add(0);
                }
            }
        }

        public void UpdateMetric( String address, int newMetric )
        {
            int index = destinationIP.IndexOf(address);
            distanceMetric.Insert(index, newMetric);
        }

        public void UpdateNextHop( String address, String newHop)
        {
            int index = destinationIP.IndexOf(address);
            nextHopIP.Insert(index, newHop);
        }

        public void IncrementTimer( String destinationAddress )
        {
            try
            {
                int index = destinationIP.IndexOf(destinationAddress);

                int duration = destinationTimes.ElementAt(index);
                duration++;
                if (duration < 5)
                    destinationTimes.Insert(index, duration);
                else
                {
                    UpdateMetric(destinationAddress, infinity);
                    UpdateNextHop(destinationAddress, "0.0.0.0");
                    destinationTimes.Insert(index, duration);
                }
            }
            catch (ArgumentOutOfRangeException e) { }
        }

        public void ResetTimer( String address )
        {
            int index = destinationIP.IndexOf(address);
            if( index < destinationTimes.Count)
                destinationTimes.Insert(index, 0);
        }

        public string GetDestination( String targetDestination )
        {
            foreach( String curDest in destinationIP )
            {
                if(curDest.Equals(targetDestination))
                    return curDest;
            }
            return null;
        }

        public String GetNextHop(String targetDestination)
        {
            int destinationIndex = destinationIP.IndexOf(targetDestination);
            if (destinationIndex != -1)
                return nextHopIP.ElementAt(destinationIndex);
            else
                return null;
        }

        public int GetDistanceMetric( String targetDestination )
        {
            int destinationIndex = destinationIP.IndexOf(targetDestination);

            if (destinationIndex != -1)
                return distanceMetric.ElementAt(destinationIndex);
            else
                return 0;
        }

        public int GetDuration( String targetDestination)
        {
            int destinationIndex = destinationIP.IndexOf(targetDestination);

            if (destinationIndex != -1 && destinationIndex < destinationTimes.Count)
                return destinationTimes.ElementAt(destinationIndex);
            else
                return 0;
        }

        public List<String> GetDestinations()
        {
            return destinationIP;
        }

        public void PrintTable( int duration )
        {
            using (StreamWriter outfile = new StreamWriter( currentNode.GetAddress().ToString() + ".txt" , true))
            {
                outfile.WriteLine("<Time>\t<Node IP>\t<Destination IP>\t<Destination Subnet Mask>\t<Next Hop>\t\t\t<Metric>\t<Timeout Duration>");

                for (int i = 0; i < destinationIP.Count(); i++)
                {
                    String currentDest = destinationIP.ElementAt(i);

                    outfile.Write(duration + "\t" + currentNode.GetAddress() + "\t" + currentDest + "\t\t255.255.255.0\t\t\t" + GetNextHop(destinationIP.ElementAt(i)) + "\t\t");

                    if (nextHopIP.ElementAt(i).Equals("0.0.0.0"))
                        outfile.Write("\t");

                    if (GetDistanceMetric(currentDest) == infinity)
                        outfile.Write("\tinfinity\t" + GetDuration(currentDest));
                    else
                        outfile.Write("\t" + GetDistanceMetric(currentDest) + "\t\t" + GetDuration(currentDest));

                    outfile.WriteLine();
                }
            }
        }

    }
}
