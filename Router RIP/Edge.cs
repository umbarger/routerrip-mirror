﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Router_RIP
{
    class Edge
    {
        private Node x;
        private Node y;
        private int weight;

        public Edge( Node a, Node b, int w)
        {
            this.x = a;
            this.y = b;
            this.weight = w;
        }

        public Node GetX()
        {
            return x;
        }

        public Node GetY()
        {
            return y;
        }

        public int GetWeight()
        {
            return weight;
        }
    }
}
