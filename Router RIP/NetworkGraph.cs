﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Router_RIP
{
    class NetworkGraph
    {
        private static HashSet<Node> networkGraph = new HashSet<Node>();
        private HashSet<String> uniqueAddresses = new HashSet<String>();

        private Random rand = new Random(DateTime.Now.Millisecond);
        private String[] nodeType = { "core", "edge" };

        public NetworkGraph( int numNodePairs )
        {
            HashSet<Edge> initialEdges = new HashSet<Edge>();
            String nodeType1;
            String nodeType2;

            for (int i = 0; i < numNodePairs; i++)
            {
                String address1 = this.GenerateIPAddress();
                String address2 = this.GenerateIPAddress();

                if (uniqueAddresses.Contains(address1) || uniqueAddresses.Contains(address2))
                    continue;

                do
                {
                    nodeType1 = GenerateNodeType();
                    nodeType2 = GenerateNodeType();
                } while (!ValidateNodeConnection(nodeType1, nodeType2));

                Node x = new Node(nodeType1, address1);
                Node y = new Node(nodeType2, address2);

                Edge edge = new Edge(x, y, GenerateWeight());

                x.AddEdge(edge);
                y.AddEdge(edge);

                initialEdges.Add(edge);
                networkGraph.Add(x);
                networkGraph.Add(y);

                uniqueAddresses.Add(address1);
                uniqueAddresses.Add(address2);
            }

            using (IEnumerator<Edge> it = initialEdges.GetEnumerator())
            {
               

                it.MoveNext();
                
                Edge curEdge = it.Current;

                while (it.MoveNext())
                {
                    Edge nextEdge = it.Current;

                    Node curX = curEdge.GetX();
                    Node curY = curEdge.GetY();

                    Node nextX = nextEdge.GetX();
                    Node nextY = nextEdge.GetY();

                    if (ValidateNodeConnection(curX.GetType().ToString(), nextX.GetType().ToString()))
                    {
                        Edge newXX = new Edge(curX, nextX, GenerateWeight());

                        curX.AddEdge(newXX);
                        nextX.AddEdge(newXX);
                    }
                    else if (ValidateNodeConnection(curX.GetType().ToString(), nextY.GetType().ToString()))
                    {
                        Edge newXY = new Edge(curX, nextY, GenerateWeight());

                        curX.AddEdge(newXY);
                        nextY.AddEdge(newXY);
                    }
                    else if (ValidateNodeConnection(curY.GetType().ToString(), nextX.GetType().ToString()))
                    {
                        Edge newYX = new Edge(curY, nextX, GenerateWeight());

                        curX.AddEdge(newYX);
                        nextY.AddEdge(newYX);
                    }
                    else if (ValidateNodeConnection(curY.GetType().ToString(), nextY.GetType().ToString()))
                    {
                        Edge newYY = new Edge(curY, nextY, GenerateWeight());

                        curX.AddEdge(newYY);
                        nextY.AddEdge(newYY);
                    }
                    curEdge = nextEdge;
                }
            }
        }

        private int GenerateWeight()
        {
            int weight;

            do
            {
                weight = rand.Next(100);
            } while (weight == 0);

            return weight;
        }

        private String GenerateNodeType()
        {
            return nodeType[rand.Next(nodeType.Length)];
        }

        private bool ValidateNodeConnection( String type1, String type2 )
        {
            // Always true because this is a simulation
            return true;
        }

        private String GenerateIPAddress()
        {
            StringBuilder ab = new StringBuilder();

            ab.Append(rand.Next(255).ToString());
            ab.Append(".");
            ab.Append(rand.Next(255).ToString());
            ab.Append(".");
            ab.Append(rand.Next(255).ToString());
            ab.Append(".");
            ab.Append(rand.Next(255).ToString());

            return ab.ToString();
        }

        public static HashSet<Node> GetNetworkGraph()
        {
            return networkGraph;
        }

        public HashSet<String> GetAddresses()
        {
            return uniqueAddresses;
        }

        public String ToString()
        {
            StringBuilder gb = new StringBuilder();

            foreach( Node n in networkGraph )
            {
                HashSet<Edge> edges = n.GetEdges();

                int counter = 0;

                foreach( Edge e in edges )
                {
                    gb.Append("\t" + e.GetX().GetAddress() + " to " + e.GetY().GetAddress() + " weight: " + e.GetWeight());

                    if (counter < edges.Count)
                        gb.Append("\n");

                    counter++;
                }
            }

           // gb.Remove(gb.Length - 1, gb.Length );

            return gb.ToString();
        }
    }
}
