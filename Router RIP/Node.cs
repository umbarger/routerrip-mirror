﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Router_RIP
{
    class Node
    {
        private String ipAddress;
        private const String SubnetMask = "255.255.255.0";
        private string type;
        private HashSet<Edge> edges = new HashSet<Edge>();
        private bool operable = true;
        private RoutingTable table;

        Random rand = new Random( DateTime.Now.Millisecond );
        
        public Node( String t, String ipAddr )
        {
            this.type = t;
            this.ipAddress = ipAddr;
        }

        public String GetAddress()
        {
            return this.ipAddress;
        }

        public String GetMask()
        {
            return SubnetMask;
        }

        public HashSet<Edge> GetEdges()
        {
            return edges;
        }

        public void AddEdge( Edge e )
        {
            edges.Add(e);
        }

        public bool IsOk()
        {
            return operable;
        }

        private void Receive( Node neighbor, RoutingTable neighborTable )
        {
            try
            {
                String neighborAddress = neighbor.GetAddress();

                List<String> neighborDestinations = neighborTable.GetDestinations();
                List<String> myDestinations = table.GetDestinations();

                // Compare the neighbor's routing table with your own... Update better routes
                foreach (String nDest in neighborDestinations)
                {
                    foreach (String myDest in myDestinations)
                    {
                        if (myDest.Equals(nDest))
                        {
                            int myMetric = table.GetDistanceMetric(myDest);
                            int nMetric = neighborTable.GetDistanceMetric(nDest);

                            if (nMetric < myMetric)
                            {
                                int newMetric;
                                int curNodeToNeighborDistance = table.GetDistanceMetric(nDest);

                                if (curNodeToNeighborDistance == (int)Int32.MaxValue)
                                    newMetric = nMetric;
                                else
                                    newMetric = nMetric + curNodeToNeighborDistance;

                                table.UpdateMetric(myDest, newMetric);
                                table.UpdateNextHop(myDest, table.GetDestination(neighborAddress));

                                table.ResetTimer(myDest);
                                table.ResetTimer(nDest);
                            }
                        }
                    }
                }

                table.ResetTimer(neighborAddress);
            }
            catch (NullReferenceException e) { }
        }

        private void Broadcast( RoutingTable t )
        {
            HashSet<Edge> edges = GetEdges();

            foreach( Edge e in edges )
            {
                Node x = e.GetX();
                Node y = e.GetY();

                Node neighbor;

                if (x.GetAddress().Equals(ipAddress))
                    neighbor = y;
                else
                    neighbor = x;

                neighbor.Receive(this, t);

                t.IncrementTimer(neighbor.GetAddress());
            }

        }

        private void RandomFail()
        {
            if (rand.Next(20) == 0)
                operable = false;
        }

        public void Run()
        {
            HashSet<Node> network = NetworkGraph.GetNetworkGraph();
            int currentIteration = 0;

            table = new RoutingTable(this, network);

            while( true )
            {
                try
                {
                    if(IsOk())
                    {
                        table.PrintTable(currentIteration);
                        Broadcast(table);
                    }
                    else
                    {
                        Console.WriteLine(ipAddress + " has failed!");
                        break;                        
                    }

                    if( Program.enableNodeFailure )
                    {
                        RandomFail();
                    }

                    Thread.Sleep(1000);
                    currentIteration++;
                } catch ( NullReferenceException e) {}
            }
        }
    }
}
